import React, { useState } from 'react';
import {
  CEOIcon,
  ManagerIcon,
  TeamIcon,
  BusinessIcon,
  CustomerSupportIcon,
} from '@components';
import Link from 'next/link';
import useWindowSize from 'hooks/useWindowSize';
const listSideBar = [
  {
    title: 'CEO',
    icon: <CEOIcon />,
    url: '/dashboard/CEO',
  },
  {
    title: 'Manager',
    icon: <ManagerIcon />,
    submenu: [
      {
        title: 'Business',
        icon: <BusinessIcon />,
        url: '/dashboard/Manager/Business',
      },
      {
        title: 'CS',
        icon: <CustomerSupportIcon />,
        url: '/dashboard/Manager/CS',
      },
    ],
  },
  {
    title: 'Team',
    icon: <TeamIcon />,
    url: '/dashboard/Team',
  },
];
function Sidebar() {
  const [openSubmenu, setOpenSubmenu] = useState(false);
  const size = useWindowSize();
  const isMobile = size.width < 786;
  if (isMobile) {
    return (
      <div className="bg-gray-800 text-gray-100 flex h-full">
        <button className="mobile-menu-button p-4 focus:outline-none">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M4 6h16M4 12h16M4 18h16"
            />
          </svg>
        </button>
        <a className="block p-4 text-white font-bold">LOGO</a>
      </div>
    );
  }
  return (
    <div className="bg-gray-800 text-blue-100 h-screen space-y-6 py-7 px-1 absolute inset-y-0 left-0 transform -translate-x-full transition xl:translate-x-0 lg:translate-x-0  xl:relative lg:relative duration-200 ease-in-out">
      {/* logo  */}
      <a
        href=""
        className="flex items-center justify-center space-x-2 text-white"
      >
        <img
          className="object-contain h-12  rounded-full"
          src="https://via.placeholder.com/150x150"
        />
        <span className="text-2xl font-extrabold">LOGO</span>
      </a>
      {/* mobile */}

      {/* search */}
      <div className="pt-2 relative  mx-auto text-gray-600 flex justify-center ">
        <input
          className="border-2 border-gray-300 bg-white h-10 px-5 w-full rounded-lg text-sm focus:outline-none"
          type="search"
          name="search"
          placeholder="Search"
        />
        <button type="submit" className="absolute  right-0 top-0 mt-4 mr-4">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
            />
          </svg>
        </button>
      </div>
      {/* nav */}
      <nav>
        {listSideBar.map((sideBar, index) => {
          if (sideBar.url) {
            return (
              <Link key={index} href={sideBar.url} passHref>
                <div className=" py-2 px-4 flex items-center transition duration-200 hover:bg-gray-700 rounded ">
                  <span className="text-base text-white mx-1 flex items-center">
                    <i>{sideBar.icon}</i>
                    <label className="mx-2">{sideBar.title}</label>
                  </span>
                  {sideBar.submenu ? (
                    openSubmenu ? (
                      <a onClick={() => setOpenSubmenu(false)}>
                        <i className="transition duration-500 ease-in-out transform -rotate-90">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-6 w-6"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              d="M15 19l-7-7 7-7"
                            />
                          </svg>
                        </i>
                      </a>
                    ) : (
                      <a onClick={() => setOpenSubmenu(true)}>
                        <i className="transition duration-500 ease-in-out transform rotate-0">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-6 w-6"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              d="M15 19l-7-7 7-7"
                            />
                          </svg>
                        </i>
                      </a>
                    )
                  ) : (
                    ''
                  )}
                </div>
              </Link>
            );
          } else {
            return (
              <>
                <a className=" py-2 px-4 flex items-center transition duration-200 hover:bg-gray-700 rounded justify-between">
                  <span className="text-base text-white mx-1 flex">
                    <i>{sideBar.icon}</i>
                    <label className="mx-2">{sideBar.title}</label>
                  </span>
                  {openSubmenu ? (
                    <button
                      onClick={() => setOpenSubmenu(false)}
                      className="transition duration-500 ease-in-out transform -rotate-90 focus:outline-none"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M15 19l-7-7 7-7"
                        />
                      </svg>
                    </button>
                  ) : (
                    <button
                      onClick={() => setOpenSubmenu(true)}
                      className="focus:outline-none	 transition duration-500 ease-in-out transform rotate-0"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M15 19l-7-7 7-7"
                        />
                      </svg>
                    </button>
                  )}
                </a>
                {sideBar.submenu ? (
                  openSubmenu ? (
                    <div className="transition duration-500 ease-in-out transform">
                      {sideBar.submenu.map((menu, index) => (
                        <Link key={index} href={menu.url}>
                          <a className=" py-2 px-8 flex items-center transition duration-200 hover:bg-gray-700 rounded  ">
                            <span className="text-base text-white mx-1 flex items-center">
                              <i>{menu.icon}</i>
                              <label className="mx-2">{menu.title}</label>
                            </span>
                            {/* <div className="flex items-center"></div> */}
                          </a>
                        </Link>
                      ))}
                    </div>
                  ) : (
                    ''
                  )
                ) : (
                  ''
                )}
              </>
            );
          }
        })}
      </nav>
    </div>
  );
}
const renderSubmenu = () => {};
export { Sidebar };
