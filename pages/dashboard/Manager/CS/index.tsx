import { AppLayout } from 'core/Layout';
import { TableRoom } from 'core/Layout/component';
import { toJS } from 'mobx';
import RoomStore from 'mobx/RoomStore';
import { Chart } from '@components';
import React, { useContext, useEffect } from 'react';
import { OptionChartCS } from 'constants/OptionCS';
import { observer } from 'mobx-react-lite';
import { useStores } from '@models';
const DashboardCSPage = observer(() => {
  const { roomStore } = useStores();
  const { listRooms, getRoomQuarter } = roomStore;
  const rooms = Array.from(listRooms.values());

  useEffect(() => {
    getRoomQuarter(1);
  }, []);
  return (
    <AppLayout>
      {rooms.map((room, index) => {
        if (room.roomId == '002') {
          return (
            <div key={index} className="my-12">
              <TableRoom label={room.title} data={room.data} />
            </div>
          );
        }
      })}
      <div className="my-20">
        <Chart options={OptionChartCS()} />
      </div>
    </AppLayout>
  );
});

export default DashboardCSPage;
