import { OptionChartBusiness } from '@constants';
import { Chart } from '@components';
import { AppLayout } from 'core/Layout';
import { TableRoom } from 'core/Layout/component';
import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { useStores } from '@models';

const DashboardBusinessPage = observer(() => {
  const { roomStore } = useStores();
  const { listRooms, getRoomQuarter } = roomStore;
  const rooms = Array.from(listRooms.values());
  useEffect(() => {
    getRoomQuarter(1);
  }, []);
  return (
    <AppLayout>
      {rooms.map((room, index) => {
        if (room.roomId == '001') {
          console.log(room);
          return (
            <div key={index} className="my-12">
              <TableRoom label={room.title} data={room.data} />
            </div>
          );
        }
      })}
      <div className="my-20">
        <Chart options={OptionChartBusiness()} />
      </div>
    </AppLayout>
  );
});

export default DashboardBusinessPage;
