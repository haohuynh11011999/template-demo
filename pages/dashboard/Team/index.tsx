import { TeamContainer } from '@containers/team';
import { AppLayout } from 'core/Layout';
import React from 'react';

function DashboardTeamPage() {
  return (
    <AppLayout>
      <TeamContainer />
    </AppLayout>
  );
}

export default DashboardTeamPage;
