# features:

- Có thể xem số liệu theo thời gian

# layouts:

- <strong>Page dành cho CEO:</strong>
  - CEO có thể xem dữ liệu sale của tất cả phòng ban
  - Biểu đồ so sánh từng phòng ban đã sale được bao nhiêu và còn bao nhiêu sẽ đạt target
- <strong>Page dành cho Manager:</strong>
- Mỗi quản lý có thể xem số lượng Sale của phòng ban mình quản lý
- Xem số lượng Sale trên mục tiêu đề ra và số lượng còn lại
- Biểu đồ biểu diễn sự tăng trưởng của phòng ban mình trong mỗi quý
- <strong>Page dành cho Team:</strong>
   - Từng thành viên có thể xem được số lượng sale trên từng mục tiêu mà họ đặt ra, % đạt được
