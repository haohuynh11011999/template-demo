import { toJS } from 'mobx';
import { types } from 'mobx-state-tree';

const listDepartment = [
  {
    id: 1,
    roomId: '001',
    title: 'Phong Kinh Doanh',
    data: [
      {
        quarter: 1,
        sale: 1020,
        target: 1200,
      },

      {
        quarter: 2,
        sale: 1020,
        target: 10320,
      },
      {
        quarter: 3,
        sale: 300,
        target: 600,
      },
      {
        quarter: 4,
        sale: 800,
        target: 2000,
      },
    ],
  },
  {
    id: 2,
    roomId: '002',
    title: 'Phong CS',
    data: [
      {
        quarter: 1,
        sale: 100,
        target: 100,
      },
      {
        quarter: 2,
        sale: 1020,
        target: 2000,
      },
      {
        quarter: 3,
        sale: 1200,
        target: 2900,
      },
      {
        quarter: 4,
        sale: 150,
        target: 800,
      },
    ],
  },
];

const DataModel = types.model({
  quarter: types.optional(types.number, 0),
  sale: types.optional(types.number, 0),
  target: types.optional(types.number, 0),
});

const RoomModel = types.model({
  id: types.optional(types.number, 0),
  roomId: types.optional(types.string, ''),
  title: types.optional(types.string, ''),
  data: types.optional(types.array(DataModel), []),
});

export const RoomStoreModel = types
  .model({
    listRooms: types.optional(types.map(RoomModel), {}),
  })
  .actions((self) => ({
    getRoomQuarter(id: number) {
      self.listRooms.clear()
      listDepartment.map((department) => {
             const newListRooms = department.data.filter((dt) => dt.quarter == id);
        self.listRooms.set(department.roomId, {
          ...department,
          data: newListRooms,
        });
      });
    },
  }));
